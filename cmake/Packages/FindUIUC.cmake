
# Add path to module list
list(PREPEND CMAKE_MODULE_PATH 
    ${CMAKE_CURRENT_LIST_DIR}/../Inc/ 
    ${CMAKE_CURRENT_LIST_DIR}/../Req/ 
    ${CMAKE_CURRENT_LIST_DIR}/../Modules/
)

# Include custom standard package
include(FindPackageStandard)

# Load using standard package finder
find_package_standard(
  NAMES UIUC
  PATHS $ENV{UIUC} $ENV{LIBUIUC}
  HEADERS "UIUC/HandboxMsg.h"
)