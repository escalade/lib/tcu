# Use the rootproject/root image as the base image
FROM rootproject/root:latest

# Update package lists and install necessary packages
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV NPROC=8

ENV LD_LIBRARY_PATH=/usr/local/lib

# Update package lists and install necessary packages
COPY Dockerfile.packages packages

RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get -y install $(cat packages | grep -v '#') && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*

ENV CXX="/usr/bin/clang++"
ENV CC="/usr/bin/clang"

# Download and install ONNX Runtime library (optional)
ENV ONNXVERSION=1.16.3
ENV ONNXARCH=linux-x64
RUN mkdir /tmp/onnxInstall && cd /tmp/onnxInstall/ && wget -O onnx_archive.nupkg https://www.nuget.org/api/v2/package/Microsoft.ML.OnnxRuntime/${ONNXVERSION} && unzip onnx_archive.nupkg
RUN cp /tmp/onnxInstall/runtimes/${ONNXARCH}/native/libonnxruntime.so /usr/local/lib/
RUN ln -s /usr/local/lib/libonnxruntime.so /usr/local/lib/libonnxruntime.so.${ONNXVERSION}
RUN cp -r /tmp/onnxInstall/build/native/include/* /usr/local/include
RUN rm -rf /tmp/onnxInstall

# Install Escalade UIUC library
RUN git clone --recursive https://gitlab.cern.ch/escalade/lib/UIUC.git /opt/uiuc
RUN cd /opt/uiuc && mkdir build && cd build && cmake ..
RUN cd /opt/uiuc/build && make -j${NPROC} && make -j${NPROC} install
RUN rm -rf /opt/uiuc
