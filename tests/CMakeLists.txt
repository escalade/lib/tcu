# Version used during development
cmake_minimum_required(VERSION 3.5)
project(TCU/Tests)

# Include custom CMake modules and utilities
list(PREPEND CMAKE_MODULE_PATH 
    ${CMAKE_CURRENT_SOURCE_DIR}/../cmake/Inc/
    ${CMAKE_CURRENT_SOURCE_DIR}/../cmake/Req/
    ${CMAKE_CURRENT_SOURCE_DIR}/../cmake/Modules/
)

include(MessageColor)        # Include utility for colored message output
include(FindPackageStandard) # Include utility for finding packages
include(ProjectArchitecture) # Include utility for project architecture
include(ParentCMakeOnly)     # Include utility for parent CMake only actions
include(LoadTests)           # Include utility for loading tests

# Output section header for Tests
message_title("Tests")

# Collect test sources and display project architecture
FILE_SOURCES(TESTS "${CMAKE_CURRENT_SOURCE_DIR}")

DUMP_ARCHITECTURE(TESTS)

# Get common path for tests
GET_COMMON_PATH(TESTPATH_COMMON TESTS)

# Add a custom target 'tests' for running tests
add_custom_target(tests 
    COMMAND ${CMAKE_CTEST_COMMAND} --rerun-failed --output-on-failure 
    COMMENT "Running tests"
)

# Find test files, prepare test targets, and link required libraries
set(TEST_TARGETS)
foreach(SRC ${TESTS})

    get_filename_component(TEST ${SRC} NAME_WE)
    get_filename_component(TESTPATH ${SRC} PATH)
    file(RELATIVE_PATH TESTPATH_REL ${TESTPATH_COMMON} ${TESTPATH})
    if(NOT "${TESTPATH_REL}" STREQUAL "")
        list(APPEND TESTPATH_REL "/")
    endif()

    message_color("-- Preparing target test `./${TESTPATH_REL}${TEST}`" COLOR GREEN)

    # Prepare test program
    add_gtest(${TEST} ${SRC} ${HEADERS})
    target_link_package(${TEST} ${LIBRARY})
    target_include_directories(${TEST} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

    # Custom `make tests`
    add_dependencies(tests ${TEST})
    
endforeach()

