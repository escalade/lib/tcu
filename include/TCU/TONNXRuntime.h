#ifndef TONNXRUNTIME_H
#define TONNXRUNTIME_H

#include <Riostream.h>
#include <vector>
#include <onnxruntime_cxx_api.h>
#include <TObject.h>

namespace TCU {
   
   class TONNXRuntime : public TObject {
        public:
            TONNXRuntime(const char* model_path);
            virtual ~TONNXRuntime();
            std::vector<float> Infer(const std::vector<float>& input_data);

        private:
            // Ort::Env env_;
            // Ort::Session session_;

            ClassDef(TONNXRuntime, 1); // Add this line for ROOT I/O
    };
}

#endif // TONNXRUNTIME_H